package softcube.model;

public class ProductRecom {
	
	private Integer item_init;
	private Integer item_recommend;
	private double score;
	
	public Integer getItem_init() {
		return item_init;
	}
	public void setItem_init(Integer item_init) {
		this.item_init = item_init;
	}
	public Integer getItem_recommend() {
		return item_recommend;
	}
	public void setItem_recommend(Integer item_recommend) {
		this.item_recommend = item_recommend;
	}
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((item_init == null) ? 0 : item_init.hashCode());
		result = prime * result
				+ ((item_recommend == null) ? 0 : item_recommend.hashCode());
		long temp;
		temp = Double.doubleToLongBits(score);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProductRecom other = (ProductRecom) obj;
		if (item_init == null) {
			if (other.item_init != null)
				return false;
		} else if (!item_init.equals(other.item_init))
			return false;
		if (item_recommend == null) {
			if (other.item_recommend != null)
				return false;
		} else if (!item_recommend.equals(other.item_recommend))
			return false;
		if (Double.doubleToLongBits(score) != Double
				.doubleToLongBits(other.score))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "ProductRecom [item_init=" + item_init + ", item_recommend="
				+ item_recommend + ", score=" + score + "]";
	}
	
	

}
