package softcube.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import softcube.model.Product;
import softcube.model.ProductRecom;
import softcube.util.DbUtil;

public class Dao {

	private Connection connection;

	public Dao() {
		connection = DbUtil.getConnection();
	}

	public List<Product> getListProducts() {
		List<Product> products = new ArrayList<Product>();

		try {
			PreparedStatement preparedStatement = connection.
					prepareStatement("select distinct id, url from product_characteristics_yakoboo_keyvalue_mv_vanya");

			ResultSet rs = preparedStatement.executeQuery();

			while (rs.next()) {
				Product product = new Product();
				product.setProductid(rs.getInt("id"));
				product.setUrl(rs.getString("url"));
				products.add(product);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return products;
	}

	public void insertT0Db(List<ProductRecom> productsRecom) {

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("insert into product_recom_yakoboo_2 (item_init, item_recommend, score) values");

		for(int i = 0; i < productsRecom.size(); i++){
			if(isTheLastProduct(productsRecom, i)){
				stringBuilder.append(String.format(" ('%s','%s','%s')",productsRecom.get(i).getItem_init(),productsRecom.get(i).getItem_recommend(),productsRecom.get(i).getScore()));

				executeQuery(stringBuilder);

			}else
			{
				stringBuilder.append(String.format(" ('%s','%s','%s'),",productsRecom.get(i).getItem_init(),productsRecom.get(i).getItem_recommend(),productsRecom.get(i).getScore()));
			}


		}

	}

	private void executeQuery(StringBuilder stringBuilder) {
		try {
			PreparedStatement preparedStatement;
			preparedStatement = connection.
					prepareStatement(stringBuilder.toString());
			preparedStatement.executeUpdate();
			connection.commit();
			preparedStatement.close();

		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

	private boolean isTheLastProduct(List<ProductRecom> productsRecom,
			int i) {
		return i == productsRecom.size() - 1;
	}


}
