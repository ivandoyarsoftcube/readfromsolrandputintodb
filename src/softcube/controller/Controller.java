package softcube.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import softcube.dao.Dao;
import softcube.model.Product;
import softcube.model.ProductRecom;

public class Controller {

	private Dao dao;

	public Controller() {
		dao = new Dao();
	}

	public void fillTable() {

		List<Product> products = dao.getListProducts();
		
		List<ProductRecom> productsRecom = new ArrayList<ProductRecom>();

		int size = products.size();

		int numberOfProduct = 0;

		for (Product product : products) {

				try {
					
					String json = getJsonFromUrl(product);
					
					JSONArray array = parseJson(json);

					for(int i = 0 ; i < array.size() ; i++){

						JSONObject ob = (JSONObject)array.get(i);
						Integer recom_id = Integer.parseInt((String) ob.get("id"));

						if(recom_id!=product.getProductid()){
							double score = (Double) ob.get("score");
							ProductRecom productRecom = createNewProductRecom(
									product, recom_id, score);
							if(isListSizeDivideInThousand(productsRecom)){
								insertDataToDbAndClearList(productsRecom);
								productsRecom.add(productRecom);
							}
							else{
								productsRecom.add(productRecom);
							}
						}
					}

				} catch (ParseException e) {
					e.printStackTrace();
				}
			 catch (MalformedURLException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}

			numberOfProduct++;

			if( isTheLastProduct(productsRecom, size, numberOfProduct)){
				insertDataToDbAndClearList(productsRecom);
			}
		}

	}

	private boolean isListSizeDivideInThousand(List<ProductRecom> productsRecom) {
		return productsRecom.size() != 0 && productsRecom.size()%1000 == 0;
	}

	private ProductRecom createNewProductRecom(Product product,
			Integer recom_id, double score) {
		ProductRecom productRecom = new ProductRecom();
		productRecom.setItem_init(product.getProductid());
		productRecom.setItem_recommend(recom_id);
		productRecom.setScore(score);
		return productRecom;
	}

	private void insertDataToDbAndClearList(List<ProductRecom> productsRecom) {
		dao.insertT0Db(productsRecom);
		productsRecom.clear();
	}

	private boolean isTheLastProduct(List<ProductRecom> productsRecom,
			int size, int j) {
		return j == size && !productsRecom.isEmpty();
	}

	private JSONArray parseJson(String json) throws ParseException {
		Object obj;
		JSONParser parser = new JSONParser();
		obj = parser.parse(json);
		JSONObject jsonObj = (JSONObject) obj;
		JSONObject jo = (JSONObject) jsonObj.get("response");
		JSONArray array = (JSONArray) jo.get("docs");
		return array;
	}

	private String getJsonFromUrl(Product product)
			throws MalformedURLException, IOException {
		String current_url = product.getUrl();
		URL url = new URL(current_url);
		URLConnection con = url.openConnection();
		InputStream in = con.getInputStream();
		String encoding = con.getContentEncoding();
		encoding = encoding == null ? "UTF-8" : encoding;
		String json = IOUtils.toString(in, encoding);
		return json;
	}

}
